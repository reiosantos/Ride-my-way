# Ride-my-way

Ride-my App is a carpooling application that provides drivers with the ability to create ride offers and passengers to join available ride offers.

[![Build Status](https://travis-ci.org/reiosantos/Ride-my-way.svg?branch=master)](https://travis-ci.org/reiosantos/Ride-my-way)
